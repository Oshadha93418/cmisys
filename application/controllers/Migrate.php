<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if($this->input->is_cli_request() == FALSE){
            show_404();
        }
        $this->load->library('migration');
        $this->load->dbforge();
    }
	public function latest()
	{
        echo "Migrate started";
        $this->migration->latest();
       echo  $this->migration->error_string().PHP_EOL;
    }
    
    public function reset()
	{
        echo "Migrate Reset started";
        $this->migration->version(0);
       echo  $this->migration->error_string().PHP_EOL;
	}
}