<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('dashboard_model');
        if(!$this->session->userdata('logged_in')) {
            redirect(site_url());
        }
    }

    public function index() {

        /* $data = $this->Dashboard_model->get_user_list(); */
        $data['users'] = $this->dashboard_model->get_user_list();

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('dashboard',$data);
        $this->load->view('frame/footer_view');
    }


    function create_customer()   {
        $this->load->view('user/create_customer');
    }

}

/* End of file */
