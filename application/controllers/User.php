<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends CI_Controller {

   public function __Construct() {
               parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(site_url());
        }

        if($this->session->userdata('role') != 'user'){
            redirect(site_url());
        }

        $this->load->model('admin_model');
    }
    

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(site_url());
        }

       

    }

    function create_customer()   {

         $data = array(
            'formTitle' => 'Create Customer',
            'title' => '<i> Commercial Micro Investment </i> > <strong> Create Customer </strong>',
            //'users' => $this->admin_model->get_user_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');      
        $this->load->view('user/create_customer' , $data);
    }


    function create_loan()   {

         $data = array(
            'formTitle' => 'Create Loan',
            'title' => ' <i> Commercial Micro Investment </i> > <strong>  Create Loan </strong>',
            //'users' => $this->admin_model->get_user_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');      
        $this->load->view('user/create_loan' , $data);
    }

     function create_voucher()   {

         $data = array(
            'formTitle' => 'Create Voucher',
            'title' => ' <i> Commercial Micro Investment </i> > <strong>  Create Voucher </strong>',
            //'users' => $this->admin_model->get_user_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');      
        $this->load->view('user/create_voucher' , $data);
    }

    function customer_list(){

        $data = array(
            'formTitle' => 'Customer Management',
            'title' => 'Customer Management',
            'users' => $this->admin_model->get_user_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('user/customer_list' , $data);
        $this->load->view('frame/footer_view');
    }

}

/* End of file */


        