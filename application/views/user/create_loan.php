 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div role="tabpanel">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="advance">
                                        
                                        
                <form>

                      <div class="row">
                      <div class="col-xs-5">
                      <div class="form-group">
                        <label for="exampleInputPassword1">CMIL Number</label> 
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="CMILN-001-000001" readonly="true">
                      </div>
                      </div>
                      </div>

                      <div class="row">
                      <div class="col-xs-5">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Customer NIC Number</label> 
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="NIC Number">
                      </div>
                      </div>
                      </div>

                      
                      <div class="row">
                      <div class="col-xs-5">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Loan Attempts</label> 
                        <input type="Number" class="form-control" id="exampleInputPassword1" placeholder="Attempts">
                      </div>
                      </div>
                     </div>

                     <div class="row">
                      <div class="col-xs-5">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Loan Amount</label> 
                        <input type="Number" class="form-control" id="exampleInputPassword1" placeholder="Amount">
                      </div>
                      </div>
                     </div>

                     <div class="row">
                      <div class="col-xs-6">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Reason</label> 
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Reason">
                      </div>
                      </div>
                      </div>

                      <div class="row">
                      <div class="col-xs-6">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Spouse</label> 
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Spouse (කලත්‍රයා)">
                      </div>
                      </div>
                      </div>

                       <div class="row">
                       <div class="col-xs-3">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Interest</label>                         
                        <select class="form-control">
                          <option>2.5%</option> 
                          <option>3%</option>
                          <option>3.5%</option>                         
                          <option>4%</option>                         
                          <option>4.5%</option>                         
                          <option>5%</option>                         
                        </select>
                        </div>
                        </div>
                        </div>     

                        <div class="row">
                       <div class="col-xs-3">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Loan Period</label>                         
                        <select class="form-control">
                          <option>Week</option> 
                          <option>2 Week</option>
                          <option>Month</option>                                              
                        </select>
                        </div>
                        </div>
                        </div>    

                        <div class="row">
                       <div class="col-xs-3">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Loan Category</label>                         
                        <select class="form-control">
                          <option>LL</option> 
                          <option>GL</option>
                          <option>BL</option>                                              
                        </select>
                        </div>
                        </div>
                        </div>    

                          <div class="row">
                       <div class="col-xs-3">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Loan Turm</label>                         
                        <select class="form-control">
                          <option>1 Year</option> 
                          <option>2 Year</option>
                          <option>3 Year</option>                                              
                        </select>
                        </div>
                        </div>
                        </div>    

                         <div class="row">
                       <div class="col-xs-3">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Collection Person</label>                         
                        <select class="form-control">
                          <option>A</option> 
                          <option>AA</option>
                          <option>AAA</option>                                              
                        </select>
                        </div>
                        </div>
                        </div>                                      
                   
                          

                          <div class="form-group">
                          <label for="exampleInputFile">Security</label>
                          <input type="file" id="exampleInputFile">
                          <p class="help-block">Attached Security Photo here.</p>
                          </div>
                    

                      <div class="row">
                      <div class="col-xs-4">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Installment</label> 
                        <input type="Number" class="form-control" id="exampleInputPassword1" placeholder="Installment">
                      </div>
                      </div>
                      </div>

                      

                
                      <button type="submit"  class="btn btn-primary">For Approvel</button>
                </form>
                                       

                                       
                                    </div>

                                
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>

       
        <!-- /#page-wrapper -->


<?php $this->load->view('frame/footer_view') ?>



       