<div class="main-panel text-center">
  <div class="card col-lg-12 col-sm-6">
  <div class="card-header">
        <div class="card-title">
          <h3><b>Create New Customer</b></h3>
        </div>
        <hr/>
  </div>
    <div class="card-body">
      <form>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Title</label>                         
                <select class="form-control">
                  <option>Mr.</option> 
                  <option>Mrs.</option>
                  <option>Miss.</option>
                  <option>Other</option>                         
                </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Customer First Name</label> 
              <input type="text" class="form-control" id="exampleInputPassword1" placeholder="First Name">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Customer Last Name</label> 
              <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Last Name">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Customer NIC Number</label> 
              <input type="text" class="form-control" id="exampleInputPassword1" placeholder="NIC Number">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Customer Birth Day</label> 
              <input type="date" class="form-control" id="exampleInputPassword1" placeholder="birthday">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Permenent Address</label> 
              <textarea class="form-control" rows="3"></textarea>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Spouse</label> 
              <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Spouse (කලත්‍රයා)">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Customer Mobile Number</label> 
              <input type="Number" class="form-control" id="exampleInputPassword1" placeholder="Mobile Number">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Job Status</label> 
              <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Job Status">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Living Center</label>                         
              <select class="form-control">
                <option>A</option> 
                <option>AB</option>
                <option>ABC</option>                         
              </select>
            </div>
          </div>
        </div>

          <button type="submit" class="btn btn-primary">Submit</button>

      </form>
    </div>
  </div>
</div>

<?php if($this->session->flashdata('success')):?>

                <div class="alert alert-success">

                <a href="#" class="close" data-dismiss="alert">&times;</a>

                <strong><?php echo $this->session->flashdata('success'); ?></strong>

                </div>

            <?php elseif($this->session->flashdata('error')):?>

                <div class="alert alert-warning">

                <a href="#" class="close" data-dismiss="alert">&times;</a>

                <strong><?php echo $this->session->flashdata('error'); ?></strong>

                </div>

            <?php endif;?>



<?php $this->load->view('frame/footer_view') ?>







       