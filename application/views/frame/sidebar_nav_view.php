            <!-- <div class="sidebar" data-background-color="white" role="navigation">
                <div class="sidebar-nav navbar-collapse" >
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href=""><i class="fa fa-home fa-fw"></i> Dashboard</a>
                        </li>
                        <?php if($this->session->userdata('role') == 'admin'): ?>
                            <li>
                                <a href="#"><i class="fa fa-user fa-fw"></i> Administrator<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li> <a href="<?=site_url('admin/user_list')?>">&raquo; User List</a> </li>
                                    <li> <a href="<?=site_url('admin/activity_log')?>">&raquo; Activity Log</a> </li>
                                </ul>
                            </li>

                            <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i> Add Category <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="<?=site_url('admin/add_centers')?>">&raquo; Centers </a> </li>
                                <li> <a href="<?=site_url('admin/add_interest')?>">&raquo; Interest</a> </li>
                                <li> <a href="<?=site_url('admin/add_loan_period')?>">&raquo; Loan Period</a> </li>
                                <li> <a href="<?=site_url('admin/add_loan_category')?>">&raquo; Loan Category</a> </li>

                            </ul>

                            <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i> Reports <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="#">&raquo; Test </a> </li>
                                <li> <a href="#">&raquo; Test</a> </li>
                                <li> <a href="#">&raquo; Test</a> </li>
                                <li> <a href="<#">&raquo; Test</a> </li>

                            </ul>
                        </li>
                        <?php endif; ?>

                        <?php if($this->session->userdata('role') == 'user'): ?>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Customer<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="<?=site_url('user/create_customer')?>">&raquo; Create Customer </a> </li>
                                <li> <a href="<?=site_url('user/customer_list')?>">&raquo; Customer List</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i> Loan<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="<?=site_url('user/create_loan')?>">&raquo; Create Loan </a> </li>
                                <li> <a href="#">&raquo; Pending / Approved Loan</a> </li>
                                <li> <a href="#">&raquo; Check Loan</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-file fa-fw"></i> Voucher<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="<?=site_url('user/create_voucher')?>">&raquo; Create Voucher </a> </li>
                                <li> <a href="#">&raquo; Test</a> </li>
                                <li> <a href="#">&raquo; Test</a> </li>
                            </ul>
                        </li>    

                        <li>
                            <a href="#"><i class="fa fa-file fa-fw"></i> Daily Task<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="#">&raquo; Collections </a> </li>
                                <li> <a href="#">&raquo; Test</a> </li>
                                <li> <a href="#">&raquo; Test</a> </li>
                            </ul>
                        </li>                        
                        <?php endif; ?>

                    </ul>
                </div>
                 /.sidebar-collapse 
            </div> -->

    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a class="simple-text" href="<?=site_url("dashboard");?>">
                    <div class="inline"> &nbsp;Commercial Micro Investment </div>
                </a>
            </div>
            <ul class="nav" id="side-menu">
                <li class="active">
                    <a href="<?=site_url("dashboard")?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <?php if($this->session->userdata('role') == 'admin'): ?>
                <li>
                    <a href="#"><i class="fa fa-user fa-fw"></i> Administrator<span class="fa arrow"></span></a>
                    <ul class="nav-second-level">
                        <li> <a href="<?=site_url('admin/user_list')?>">&raquo; User List</a> </li>
                        <li> <a href="<?=site_url('admin/activity_log')?>">&raquo; Activity Log</a> </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-file-text fa-fw"></i> Add Category <span class="fa arrow"></span></a>
                    <ul class="nav-second-level">
                        <li> <a href="<?=site_url('admin/add_centers')?>">&raquo; Centers </a> </li>
                        <li> <a href="<?=site_url('admin/add_interest')?>">&raquo; Interest</a> </li>
                        <li> <a href="<?=site_url('admin/add_loan_period')?>">&raquo; Loan Period</a> </li>
                        <li> <a href="<?=site_url('admin/add_loan_category')?>">&raquo; Loan Category</a> </li>

                    </ul>

                    <li>
                    <a href="#"><i class="fa fa-file-text fa-fw"></i> Reports <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="#">&raquo; Test </a> </li>
                        <li> <a href="#">&raquo; Test</a> </li>
                        <li> <a href="#">&raquo; Test</a> </li>
                        <li> <a href="<#">&raquo; Test</a> </li>

                    </ul>
                </li>
                <?php endif; ?>
                <?php if($this->session->userdata('role') == 'user'): ?>
                <li>
                    <a href="#"><i class="fa fa-user fa-fw"></i> Customer<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="<?=site_url('user/create_customer')?>">&raquo; Create Customer </a> </li>
                        <li> <a href="<?=site_url('user/customer_list')?>">&raquo; Customer List</a> </li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-file-text fa-fw"></i> Loan<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="<?=site_url('user/create_loan')?>">&raquo; Create Loan </a> </li>
                        <li> <a href="#">&raquo; Pending / Approved Loan</a> </li>
                        <li> <a href="#">&raquo; Check Loan</a> </li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-file fa-fw"></i> Voucher<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="<?=site_url('user/create_voucher')?>">&raquo; Create Voucher </a> </li>
                        <li> <a href="#">&raquo; Test</a> </li>
                        <li> <a href="#">&raquo; Test</a> </li>
                    </ul>
                </li>    

                <li>
                    <a href="#"><i class="fa fa-file fa-fw"></i> Daily Task<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="#">&raquo; Collections </a> </li>
                        <li> <a href="#">&raquo; Test</a> </li>
                        <li> <a href="#">&raquo; Test</a> </li>
                    </ul>
                </li>                        
                <?php endif; ?>
            </ul>
    	</div>
    </div>
            <!-- /.navbar-static-side -->
</nav>