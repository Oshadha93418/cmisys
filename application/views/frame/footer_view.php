    <div class="col-lg-12 text-center" style="padding:5px;"><small>&copy; 2018 by <a target="_blank" href="http://facebook.com/siosolutions">SioSolutions</a></small></div>
    </div>
    <!-- /#wrapper -->
    <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">CHANGE PASSWORD (<?=$this->session->userdata('email')?>)</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <label class="error" id="error_changePassword">invalid current password</label>
                            <label class="error" id="error_changePassword2">password must be at least 8 characters (alphanumeric or special characters)</label>
                        </div>
                    </div>
                    &nbsp;
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Current Password</label> &nbsp;&nbsp;
                                <label class="error" id="error_currentPassword"> field is required.</label>
                                <input class="form-control" id="currentPassword" placeholder="Current Password" name="currentPassword" type="password" autofocus>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>New Password</label> &nbsp;&nbsp;
                                <label class="error" id="error_newPassword"> field is required.</label>
                                <label class="error" id="error_newPassword2"> password not match</label>
                                <input class="form-control" id="newPassword" placeholder="New Password" name="newPassword" type="password" autofocus>
                            </div> 
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Confirm New Password</label> &nbsp;&nbsp;
                                <input class="form-control" id="confirmNewPassword" placeholder="Confirm New Password" name="confirmNewPassword" type="password" autofocus>
                            </div> 
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button id="changePasswordSubmit" type="button" class="btn btn-primary">UPDATE</button>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->

    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=base_url()?>assets/js/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap-editable.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap-editable.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=base_url()?>assets/js/sb-admin-2.js"></script>

    <script>
    $(document).ready(function () {

        $('#sidebarCollapse').on('click', function () {
            $('.sidebar').toggleClass('active');
            $('.main-panel').toggleClass('active');
            $('.navbar-fixed').toggleClass('active');
        });

        });
        $(document).ready(function () {

            $('#sidebarMobileCollapse').on('click', function () {
                $('.sidebar').toggleClass('active');
                
            });

        });
    </script>



</body>

</html>