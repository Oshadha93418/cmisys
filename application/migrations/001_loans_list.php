<?php
class Migration_Loans_list extends CI_Migration {
    public function up(){
        $this->dbforge->add_field(
            array(
                'id'=>array(
                    'type'=>'INT',
                    'unsigned'=>'TRUE',
                    'auto_incriment'=>'TRUE'
                ),
                'loan_title'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'loan_amount'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'note'=>array(
                    'type'=>'text'
                ),
                'time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
                );
                $this->dbforge->add_key('id',TRUE);
                $this->dbforge->create_table('loans');

    }

    public function down(){
        $this->dbforge->drop_table('loans');
    }
}