<?php
class Migration_Loan_customers extends CI_Migration {
    public function up(){
        $this->dbforge->add_field(
            array(
                'id'=>array(
                    'type'=>'INT',
                    'unsigned'=>'TRUE',
                    'auto_incriment'=>'TRUE'
                ),
                'title'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_fname'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_lname'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_nic_number'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_address'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_mobile_number'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_bdate'=>array(
                    'type'=>'DATETIME'
                ),
                'customer_designation'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'customer_living_center'=>array(
                    'type'=>'VARCHAR',
                    'constraint'=>250
                ),
                'note'=>array(
                    'type'=>'text'
                ),
                'time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
                );
                $this->dbforge->add_key('id',TRUE);
                $this->dbforge->create_table('customers');

    }

    public function down(){
        $this->dbforge->drop_table('customers');
    }
}